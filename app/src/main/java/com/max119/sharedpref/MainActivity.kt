package com.max119.sharedpref

import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.max119.sharedpref.data.User
import com.max119.sharedpref.extentions.setValue
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    var sp:SharedPreferences ? =null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        sp=applicationContext.getSharedPreferences("Myuser", MODE_PRIVATE)
        button.setOnClickListener{
            click()
        }
        btn_view.setOnClickListener{
            val intent = Intent(this,ShowActivity::class.java)
            startActivity(intent)
        }

    }
    private fun click(){
        val name = et_name.text.toString()
        var email=et_email.text.toString()
        var phone = et_phone.text.toString()
        var pass:String=et_email.text.toString()
        var address = et_address.text.toString()
        var gender = if (radio_male.isChecked) {
          "Male"
        } else {
           "Female"
        }
        sp?.setValue("name",name)
        sp?.setValue("email",email)
        sp?.setValue("phone",phone)
        sp?.setValue("pass",pass)
        sp?.setValue("address",address)
        sp?.setValue("gender",gender)
        Toast.makeText(this,"$email $pass",Toast.LENGTH_LONG).show()
    }
}