package com.max119.sharedpref

import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.max119.sharedpref.extentions.get
import kotlinx.android.synthetic.main.activity_show.*

class ShowActivity : AppCompatActivity() {

    lateinit var sp: SharedPreferences
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_show)
        sp=applicationContext.getSharedPreferences("Myuser", MODE_PRIVATE)
        txt_name.text = sp.get<String>("name")
        txt_mail.text = sp.get<String>("email")
        txt_phone.text = sp.get<String>("phone")
        txt_address.text = sp.get<String>("address")
        txt_gender.text = sp.get<String>("gender")
    }
}