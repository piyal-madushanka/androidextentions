package com.max119.sharedpref.data

import java.net.Inet4Address

data class User(
    var name:String? = null,
    var address:String? = null,
    var gmail:String? = null,
    var phone:String? = null,
    var gender:String? = null
)
